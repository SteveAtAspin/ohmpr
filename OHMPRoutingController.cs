using System;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;

namespace OHMPRoutingService
{
    public partial class OHMPRoutingController : ServiceBase
    {
        #region Declarations
        SqlConnection _ohconn;
        SqlConnection _mpconn1;
        SqlConnection _mpconn2;
        SqlCommand cmdSource;
        SqlCommand cmdOrderSource;
        SqlCommand cmdDest;
        SqlDataReader dr = null;
        private System.Timers.Timer timer1 = new System.Timers.Timer();
        private DateTime _startTime;
        private DateTime _endTime;
        private List<string> _tables;
        private string _server;
        private string _authentication;
        private string _username;
        private string _password;
        private string _ohdatabase;
        private string _mpdatabase1;
        private string _mpdatabase2;
        private string _warehouse1;
        private string _warehouse2;
        private string mpversion = "";
        private int _lastMinuteUpdate = -1;
        private decimal _idleDelay = 0;
        private bool _checkforfailed = true;
        #endregion

        public OHMPRoutingController()
        {
            InitializeComponent();

            if (!EventLog.SourceExists(Constants.defaultEventSource))
            {
                EventLog.CreateEventSource(Constants.defaultEventSource, Constants.defaultEventLog);
            }
            ASEventLog.Source = Constants.defaultEventSource;
            ASEventLog.Log = Constants.defaultEventLog;
            ASEventLog.MaximumKilobytes = 1024;
            ASEventLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 7);
            ASEventLog.WriteEntry("starting service");
        }

        //private void OpenSQLConnections(SqlConnection ohconn, SqlConnection mpconn1, SqlConnection mpconn2)
        //{
        //    try
        //    {
        //        SqlConnection _ohconn = new SqlConnection();
        //        SqlConnection _mpconn1 = new SqlConnection();
        //        SqlConnection _mpconn2 = new SqlConnection();
        //        _ohconn = CreateConnection(_server, _authentication, _ohdatabase, _username, _password);
        //        _ohconn.Open();
        //        _mpconn1 = CreateConnection(_server, _authentication, _mpdatabase1, _username, _password);
        //        _mpconn1.Open();
        //        _mpconn2 = CreateConnection(_server, _authentication, _mpdatabase2, _username, _password);
        //        _mpconn2.Open();

        //    }
        //    catch (SqlException ex)
        //    {
        //        ASEventLog.WriteEntry("Aspin OHMP Routing Controller could not open the SQL connections.\r\n\r\n" +
        //                              "Error was: " + ex.Message + "\r\n\r\n" + ex.StackTrace +
        //                              "Please check settings and try again.");
        //        this.Stop();
        //    }
        //}

        protected override void OnStart(string[] args)
        {
            // ****************************************************************************
            // This allows time to attach the debugger.  Uncomment if needed...
            System.Threading.Thread.Sleep(20000); 
            // ****************************************************************************

            ASEventLog.WriteEntry(Constants.serviceDescription + " Starting...");
            try
            {

                LoadXML();
                
                try
                {
                    _ohconn = CreateConnection(_server, _authentication, _ohdatabase, _username, _password);
                    _ohconn.Open();
                    _mpconn1 = CreateConnection(_server, _authentication, _mpdatabase1, _username, _password);
                    _mpconn1.Open();
                    _mpconn2 = CreateConnection(_server, _authentication, _mpdatabase2, _username, _password);
                    _mpconn2.Open();

                }
                catch (SqlException ex)
                {
                    ASEventLog.WriteEntry(Constants.serviceDescription + " could not open the SQL connections.\r\n\r\n" +
                                          "Error was: " + ex.Message + "\r\n\r\n" + ex.StackTrace +
                                          "Please check settings and try again.");
                    this.Stop();
                }

                _tables = GetSQLTables(_ohconn);   // List of tables in MinipickOH database
                WriteMPVersion(mpversion, _ohconn);

                timer1.Interval = (Convert.ToDouble(_idleDelay) * 1000);// 10000;

                timer1.Elapsed += new System.Timers.ElapsedEventHandler(OHTimerElapsed);
                timer1.Enabled = true;

                ASEventLog.WriteEntry(Constants.serviceDescription + " Started");

            }
            catch (Exception ex)
            {
                ASEventLog.WriteEntry(Constants.serviceDescription + " could not be started.\r\n\r\n" +
                                      "Error was: " + ex.Message + "\r\n\r\n" + ex.StackTrace +
                                      "Please check settings and try again.");
                this.Stop();
            }
        }

        private void WriteMPVersion(string mpversion, SqlConnection source)
        {
            cmdSource = new SqlCommand("update tControl set version = '" + mpversion + "'", source);
            cmdSource.ExecuteNonQuery();
            cmdSource = new SqlCommand("update tControl set Restarts = Restarts + 1", source);
            cmdSource.ExecuteNonQuery();
            cmdSource.Dispose();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.  
            timer1.Enabled = false;

            // close down SQL connections
            if (_ohconn.State == ConnectionState.Open)
                _ohconn.Close();
            if (_mpconn1.State == ConnectionState.Open)
                _mpconn1.Close();
            if (_mpconn2.State == ConnectionState.Open)
                _mpconn2.Close();
            _ohconn.Dispose();
            _mpconn1.Dispose();
            _mpconn2.Dispose();

            ASEventLog.WriteEntry(Constants.serviceDescription + " Stopped.");
        }

        private void OHTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer1.Enabled = false;

            if (_checkforfailed)    // Check for orphan records from a failed transfer last time it was started.
            {
                CheckForFailedTransfers();
                _checkforfailed = false;
            }

            // Do processing stuff here
            DateTime dt = DateTime.Now;
            if (!((dt.Hour < _startTime.Hour || dt.Hour > _endTime.Hour)
                    || (dt.Hour == _startTime.Hour && dt.Minute < _startTime.Minute)
                    || (dt.Hour == _endTime.Hour && dt.Minute > _endTime.Minute))
                    )
            {
                // within start and stop range

                //if (_lastMinuteUpdate != dt.Minute)
                //{
                    // We have a candidate time for the schedule
                    ProcessOHData();
                    _lastMinuteUpdate = dt.Minute;
                //}

                if (_lastMinuteUpdate != dt.Minute)
                    _lastMinuteUpdate = -1;
            }
            //_mpconn2.Close();
            //_mpconn1.Close();
            //_ohconn.Close();
            System.GC.Collect();
            timer1.Enabled = true;
        }

        private void CheckForFailedTransfers()
        {
            Int32 dudRecs = 0;

            // check for timed out connections
            if (_ohconn.State != ConnectionState.Open)
                _ohconn.Open();
            if (_mpconn1.State != ConnectionState.Open)
                _mpconn1.Open();
            if (_mpconn2.State != ConnectionState.Open)
                _mpconn2.Open();

            foreach (string _table in _tables)
            {

                if (_table.Length > 4 && (_table.Substring(0, 4).ToLower() == "t_in"))
                {
                    try
                    {
                        cmdSource = new SqlCommand("SELECT COUNT(*) FROM \"" + _table + "\" WHERE UpdateFlag = 22", _mpconn1);
                        dudRecs += Convert.ToInt32(cmdSource.ExecuteScalar());
                        cmdSource = new SqlCommand("SELECT COUNT(*) FROM \"" + _table + "\" WHERE UpdateFlag = 22", _mpconn2);

                        dudRecs += Convert.ToInt32(cmdSource.ExecuteScalar());
                    }
                    catch 
                    {
                        // Otter House have a habit of adding temporary tables to the database.
                        // This results in errors

                        cmdSource.Dispose();
                        continue;
                    }
                }
                if (_table.Length > 4 && (_table.Substring(0, 4).ToLower() == "t_in"))
                {
                    cmdSource = new SqlCommand("SELECT COUNT(*) FROM \"" + _table + "\" WHERE UpdateFlag = 22", _ohconn);
                    dudRecs += Convert.ToInt32(cmdSource.ExecuteScalar());
                }
                if (dudRecs > 0)
                {
                    ASEventLog.WriteEntry("There are records in the databases that indicate a failed transfer\r\nPlease call Aspin Support");
                    return;
                }
                cmdSource.Dispose();

            }
        }

        private void ProcessOHData()
        {
            // check for timed out connections
            if (_ohconn.State != ConnectionState.Open)
                _ohconn.Open();
            if (_mpconn1.State != ConnectionState.Open)
                _mpconn1.Open();
            if (_mpconn2.State != ConnectionState.Open)
                _mpconn2.Open(); 

            foreach (string _table in _tables)
            {
                if (_table.Trim().ToLower() == "t_in_soheader" ) // Only start copy on SOHeader. SODetail will be linked 
                {
                    TransferInOrders(_table);
                }
                else
                {
                    if (_table.Length > 3 && _table.Substring(0, 4).ToLower() == "t_in" && _table.ToLower().Trim() != "t_in_sodetail")
                    {
                        TransferInData(_table);
                    }
                    else
                    {
                        if (_table.Length > 7 && _table.Substring(0, 8).ToLower() == "t_out_so") // only send back orders
                        {
                            TransferOutOrders(_table, _mpconn1, _ohconn);
                            TransferOutOrders(_table, _mpconn2, _ohconn);
                        }
                    }
                }
            }

            // Belt and braces. Loop through the t_IN tables in the Minipick databases and t_OUT tables in the Dimensions database
            // setting UpdateFlag = 22 where UpdateFlag = 21.
            // This will prevent failed records from being processed in the next pass.
            foreach (string _table in _tables)
            {
                if (_table.Length > 4 && (_table.Substring(0, 4).ToLower() == "t_in"))
                {
                    cmdSource = new SqlCommand("UPDATE " + _table + " SET UpdateFlag = 22 WHERE UpdateFlag = 21", _mpconn1);
                    try
                    {
                        cmdSource.ExecuteNonQuery();
                        cmdSource = new SqlCommand("UPDATE " + _table + " SET UpdateFlag = 22 WHERE UpdateFlag = 21", _mpconn2);
                        cmdSource.ExecuteNonQuery();
                    }
                    catch
                    {
                        // Otter House add tables to the database so ski errors caused by those
                        cmdSource.Dispose();
                        continue;
                    }
                }
                if (_table.Length > 5 && (_table.Substring(0, 5).ToLower() == "t_out"))
                {
                    cmdSource = new SqlCommand("UPDATE " + _table + " SET UpdateFlag = 22 WHERE UpdateFlag = 21", _ohconn);
                    cmdSource.ExecuteNonQuery();
                    cmdSource.Dispose();
                }
            }
        }

        /// <summary>
        /// Send t_in sales orders to 2 Minipick databases
        /// </summary>
        /// <param name="_table">Table name</param>
        private void TransferInOrders(string _table)    // _table will only be t_IN_SOHeader
        {
             // check for timed out connections
            if (_ohconn.State != ConnectionState.Open)
                _ohconn.Open();
            if (_mpconn1.State != ConnectionState.Open)
                _mpconn1.Open();
            if (_mpconn2.State != ConnectionState.Open)
                _mpconn2.Open();

            List<string> L = new List<string>();
            // Using freetype30 as the warehouse switch. Request by Ben on 11/06/2020
            cmdSource = new SqlCommand("SELECT OrderID, Freetype30, UpdateFlag FROM \"" + _table + "\" WHERE updateflag = 10", _ohconn);
            dr = cmdSource.ExecuteReader();
            while (dr.Read())
            {
                L.Add(dr.GetString(1).Trim() + "~" + dr.GetString(0).Trim()); 
                // Cannot have an active reader with the transaction in TransferOrder()
                // https://stackoverflow.com/questions/2113498/sqlexception-from-entity-framework-new-transaction-is-not-allowed-because-ther
            }
            dr.Close();
            cmdSource.Dispose();
            dr.Dispose();
            foreach (string whsorderid in L)
            {
                string[] s = whsorderid.Split('~');

                // check warehouse field against config
                if (s[0] == _warehouse1)
                {
                    TransferOrder(s[1], _mpconn1, _ohconn);
                }
                if (s[0] == _warehouse2)
                {
                    TransferOrder(s[1], _mpconn2, _ohconn);
                }
            }

            // Just to be safe we should repeat for t_IN_SODetail records for those orders in case an order was being exported during the previous loop
            foreach (string whsorderid in L)
            {
                string[] s = whsorderid.Split('~');

                // check warehouse field against config
                if (s[0] == _warehouse1)
                {
                    TransferOrder(s[1], _mpconn1, _ohconn);
                }
                if (s[0] == _warehouse2)
                {
                    TransferOrder(s[1], _mpconn2, _ohconn);
                }
            }
            L = null;
        }

        /// <summary>
        /// Transfer a single order from one database to another
        /// </summary>
        /// <param name="orderid">OrderID</param>
        /// <param name="dest">Destination database connector</param>
        /// <param name="source">Source database connector</param>
        private void TransferOrder(string orderid, SqlConnection dest, SqlConnection source)
        {
            SqlTransaction t = source.BeginTransaction(IsolationLevel.RepeatableRead);
            try
            {
                cmdOrderSource = new SqlCommand("UPDATE t_IN_SODetail SET UpdateFlag = 21 WHERE OrderId = '" + orderid + "' AND UpdateFlag = 10", source, t);
                cmdOrderSource.ExecuteNonQuery();
                cmdOrderSource.Dispose();
                cmdOrderSource = new SqlCommand("UPDATE t_IN_SOHeader SET UpdateFlag = 21 WHERE OrderId = '" + orderid + "' AND UpdateFlag = 10", source, t);
                cmdOrderSource.ExecuteNonQuery();
                cmdOrderSource.Dispose();
                cmdOrderSource = new SqlCommand("SELECT * from t_IN_SODetail WHERE OrderId = '" + orderid + "' AND UpdateFlag = 21", source, t);
                using (SqlDataReader rdr = cmdOrderSource.ExecuteReader())
                {
                    try
                    {
                        using (SqlBulkCopy bc = new SqlBulkCopy(dest))
                        {
                            bc.DestinationTableName = "t_IN_SODetail";
                            bc.BatchSize = 50;
                            bc.WriteToServer(rdr);
                            bc.Close();           
                        }
                    }
                    catch (SqlException sex)
                    {
                        t.Rollback();
                        t.Dispose();
                        try
                        {
                            cmdDest = new SqlCommand("DELETE FROM  t_IN_SODetail WITH (READPAST) WHERE OrderId = '" + orderid + "' AND UpdateFlag=21", dest);
                            cmdDest.ExecuteNonQuery();
                            cmdDest.Dispose();
                        }
                        catch { }
                        ASEventLog.WriteEntry("Transfer IN order " + orderid + " FAILED in detail copy\r\n\r\n" +
                                              "Error was: " + sex.Message + "\r\n\r\n" + sex.StackTrace);
                        return;
                    }
                }
                cmdOrderSource.Dispose();
                
                cmdOrderSource = new SqlCommand("SELECT * from t_IN_SOHeader WHERE OrderId = '" + orderid + "' AND UpdateFlag = 21", source, t);
                using (SqlDataReader rdr = cmdOrderSource.ExecuteReader())
                {
                    try
                    {
                        using (SqlBulkCopy bc = new SqlBulkCopy(dest))
                        {
                            bc.DestinationTableName = "t_IN_SOHeader";
                            bc.BatchSize = 50;
                            bc.WriteToServer(rdr);
                            bc.Close();
                        }
                    }
                    catch (SqlException sex)
                    {
                        t.Rollback();
                        t.Dispose();
                        try
                        {
                            cmdDest = new SqlCommand("DELETE FROM  t_IN_SOHeader WITH (READPAST) WHERE UpdateFlag=21 AND OrderId = '" + orderid + "'", dest);
                            cmdDest.ExecuteNonQuery();
                        }
                        catch { }
                        try
                        {
                            cmdDest = new SqlCommand("DELETE FROM  t_IN_SODetail WITH (READPAST) WHERE UpdateFlag=21 AND OrderId = '" + orderid + "'", dest);
                            cmdDest.ExecuteNonQuery();
                        }
                        catch { }
                        ASEventLog.WriteEntry("Transfer IN order " + orderid + " FAILED in header copy\r\n\r\n" +
                                              "Error was: " + sex.Message + "\r\n\r\n" + sex.StackTrace);
                        return;
                    }
                }
                cmdOrderSource.Dispose();
                cmdDest = new SqlCommand("UPDATE t_IN_SODetail WITH (READPAST) SET UpdateFlag=10 WHERE UpdateFlag=21 AND OrderId = '" + orderid + "'", dest);
                cmdDest.ExecuteNonQuery();
                cmdDest.Dispose();
                cmdDest = new SqlCommand("UPDATE t_IN_SOHeader WITH (READPAST) SET UpdateFlag=10 WHERE UpdateFlag=21 AND OrderId = '" + orderid + "'", dest);
                cmdDest.ExecuteNonQuery();
                cmdDest.Dispose();
                // set source status to processed
                cmdSource = new SqlCommand("UPDATE t_IN_SODetail WITH (READPAST) SET UpdateFlag=80 WHERE UpdateFlag=21 AND OrderId = '" + orderid + "'", source, t);
                cmdSource.ExecuteNonQuery();
                cmdSource.Dispose();
                cmdSource = new SqlCommand("UPDATE t_IN_SOHeader WITH (READPAST) SET UpdateFlag=80 WHERE UpdateFlag=21 AND OrderId = '" + orderid + "'", source, t);
                cmdSource.ExecuteNonQuery();
                t.Commit();
                t.Dispose();
                cmdSource.Dispose();

            }
            catch (SqlException ex)
            {
                t.Rollback();
                ASEventLog.WriteEntry("Transfer IN order " + orderid + " FAILED\r\n\r\n" +
                                      "Error was: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                return;
            };

        }

        /// <summary>
        /// Transfer Minipick sales orders back to Dimensions DB
        /// </summary>
        /// <param name="_table"></param>
        private void TransferOutOrders(string _table, SqlConnection source, SqlConnection dest)
        {
            // check for timed out connections
            if (_ohconn.State != ConnectionState.Open)
                _ohconn.Open();
            if (_mpconn1.State != ConnectionState.Open)
                _mpconn1.Open();
            if (_mpconn2.State != ConnectionState.Open)
                _mpconn2.Open();

            SqlTransaction t = source.BeginTransaction(IsolationLevel.RepeatableRead);
            try
            {
                cmdSource = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=21 WHERE UpdateFlag=10", source, t);
                cmdSource.ExecuteNonQuery();
                if (!BulkCopyToMP(_table, source, dest, t))
                {
                    t.Rollback();
                    t.Dispose();
                    try
                    {
                        cmdDest = new SqlCommand("DELETE FROM  \"" + _table + "\" WITH (READPAST) WHERE UpdateFlag=21", dest);
                        cmdDest.ExecuteNonQuery();
                    }
                    catch { }
                    cmdSource.Dispose();
                    cmdDest.Dispose();                
                    return;
                }
                cmdSource.Dispose();

                // Set statuses to ready for import
                cmdDest = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=10 WHERE UpdateFlag=21", dest);
                cmdDest.ExecuteNonQuery();
                cmdDest.Dispose(); 
                // set source status to processed
                cmdSource = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=80 WHERE UpdateFlag=21", source, t);
                cmdSource.ExecuteNonQuery();
                t.Commit();
                t.Dispose();
                cmdSource.Dispose();
            }
            catch (SqlException ex)
            {
                t.Rollback();
                t.Dispose();
                ASEventLog.WriteEntry("Transfer IN table " + _table + " FAILED\r\n\r\n" +
                                      "Error was: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                cmdSource.Dispose();
                cmdDest.Dispose();
                return;
            };
        }

        /// <summary>
        /// Transfer t_In tables excluding sales orders
        /// </summary>
        /// <param name="_table">Table name</param>
        private void TransferInData(string _table)
        {
            // check for timed out connections
            if (_ohconn.State != ConnectionState.Open)
                _ohconn.Open();
            if (_mpconn1.State != ConnectionState.Open)
                _mpconn1.Open();
            if (_mpconn2.State != ConnectionState.Open)
                _mpconn2.Open();

            SqlTransaction t = _ohconn.BeginTransaction(IsolationLevel.RepeatableRead);
            try
            {
                cmdSource = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=21 WHERE UpdateFlag=10", _ohconn, t);
                cmdSource.ExecuteNonQuery();
                if (!BulkCopyToMP(_table, _ohconn, _mpconn1, t))
                {
                    t.Rollback();
                    t.Dispose();
                    try
                    {
                        cmdDest = new SqlCommand("DELETE FROM  \"" + _table + "\" WITH (READPAST) WHERE UpdateFlag=21", _mpconn1);
                        cmdDest.ExecuteNonQuery();
                    }
                    catch { }
                    try
                    {
                        cmdDest = new SqlCommand("DELETE FROM  \"" + _table + "\" WITH (READPAST) WHERE UpdateFlag=21", _mpconn2);
                        cmdDest.ExecuteNonQuery();

                    }
                    catch { }
                    cmdSource.Dispose();
                    cmdDest.Dispose();
                    return;
                }
                
                if (!BulkCopyToMP(_table, _ohconn, _mpconn2, t))
                {
                    t.Rollback();
                    t.Dispose();
                    try
                    {
                        cmdDest = new SqlCommand("DELETE FROM  \"" + _table + "\" WITH (READPAST) WHERE UpdateFlag=21", _mpconn1);
                        cmdDest.ExecuteNonQuery();
                    }
                    catch { }
                    try
                    {
                        cmdDest = new SqlCommand("DELETE FROM  \"" + _table + "\" WITH (READPAST) WHERE UpdateFlag=21", _mpconn2);
                        cmdDest.ExecuteNonQuery();

                    }
                    catch { }
                    cmdSource.Dispose();
                    cmdDest.Dispose();
                    return;
                }
                // Set statuses to ready for import
                cmdDest = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=10 WHERE UpdateFlag=21", _mpconn1);
                cmdDest.ExecuteNonQuery();
                cmdDest.Dispose();
                cmdDest = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=10 WHERE UpdateFlag=21", _mpconn2);
                cmdDest.ExecuteNonQuery();
                cmdDest.Dispose();
                // set source status to processed
                cmdSource = new SqlCommand("UPDATE " + _table + " WITH (READPAST) SET UpdateFlag=80 WHERE UpdateFlag=21", _ohconn, t);
                cmdSource.ExecuteNonQuery();
                t.Commit();
                t.Dispose();
                cmdSource.Dispose();
            }
            catch (SqlException ex)
            {
                t.Rollback();
                t.Dispose();
                ASEventLog.WriteEntry("Transfer IN table " + _table + " FAILED\r\n\r\n" +
                                      "Error was: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                cmdSource.Dispose();
                cmdDest.Dispose();
                return;
            }
        }

        private bool BulkCopyToMP(string table, SqlConnection source, SqlConnection dest, SqlTransaction trans)
        {
            // check for timed out connections
            if (_ohconn.State != ConnectionState.Open)
                _ohconn.Open();
            if (_mpconn1.State != ConnectionState.Open)
                _mpconn1.Open();
            if (_mpconn2.State != ConnectionState.Open)
                _mpconn2.Open();

            cmdSource = new SqlCommand("SELECT * FROM \"" + table + "\" WHERE UpdateFlag = 21", source, trans);
            using (SqlDataReader rdr = cmdSource.ExecuteReader())
            {
                try
                {
                    using (SqlBulkCopy bc = new SqlBulkCopy(dest))
                    {
                        bc.DestinationTableName = table;
                        bc.BatchSize = 50;
                        bc.WriteToServer(rdr);
                        bc.Close();                       
                    }

                }
                catch (SqlException sex)
                {
                    trans.Rollback();
                    ASEventLog.WriteEntry("Transfer IN table " + table + " FAILED in bulk copy\r\n\r\n" +
                                          "Error was: " + sex.Message + "\r\n\r\n" + sex.StackTrace);
                    cmdSource.Dispose();
                    return false;
                }

            }

            cmdSource.Dispose();
            return true;
        }

        private List<string> GetSQLTables( SqlConnection _srcconn)
        {
            System.Collections.Generic.List<string> tables = new List<string>();
            DataTable dt = _srcconn.GetSchema("Tables");
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tables.Add(tablename);
            }
            return tables;
        }
            
        private static SqlConnection CreateConnection(string server, string security, string database, string user, string pass)
        {
            SqlConnection con = new SqlConnection();
            if (security == "Windows Authentication")
            {
                con.ConnectionString = string.Format(
                    @"Server={0}; Integrated security={1}; Initial Catalog={2}; MultipleActiveResultSets=True; Application Name=OHMPRouter",
                    server, "SSPI", database);
            }
            else
            {
                con.ConnectionString = string.Format(
                    @"Server={0}; Initial Catalog={1}; MultipleActiveResultSets=True; User Id={2}; Password={3}; Application Name=OHMPRouter;",
                    server, database, user, pass);
            }
            return con;
        }
              
        private static void logErr(string sFileName, Exception ex)
        {
            StreamWriter sw = new StreamWriter(Constants.appDatapathWithSlash + @"\errfiles\errfiles.log", true, Encoding.ASCII);
            sw.WriteLine(DateTime.Now.ToString());
            sw.WriteLine(sFileName);
            sw.WriteLine(ex.Message);
            sw.Flush();
            sw.Close();
        }

        private void LoadXML()
        {
            _startTime = (OtherStuff.LoadXMLconfig("StartTime", "OHMP-Routing") == "") ? Convert.ToDateTime("00:00") : Convert.ToDateTime(OtherStuff.LoadXMLconfig("StartTime", "OHMP-Routing"));
            _endTime = (OtherStuff.LoadXMLconfig("EndTime", "OHMP-Routing") == "") ? Convert.ToDateTime("23:59") : Convert.ToDateTime(OtherStuff.LoadXMLconfig("EndTime", "OHMP-Routing"));
            _server = OtherStuff.LoadXMLconfig("server", "OHMP-Routing");
            _authentication = OtherStuff.LoadXMLconfig("authentication", "OHMP-Routing");
            _username = (OtherStuff.LoadXMLconfig("username", "OHMP-Routing") == "") ? "" : Crypto.Decrypt(OtherStuff.LoadXMLconfig("username", "OHMP-Routing"), "OHMP-Routing");
            _password = (OtherStuff.LoadXMLconfig("password", "OHMP-Routing") == "") ? "" : Crypto.Decrypt(OtherStuff.LoadXMLconfig("password", "OHMP-Routing"), "OHMP-Routing");
            _ohdatabase = OtherStuff.LoadXMLconfig("ohdatabase", "OHMP-Routing");
            _mpdatabase1 = OtherStuff.LoadXMLconfig("mpdatabase1", "OHMP-Routing");
            _mpdatabase2 = OtherStuff.LoadXMLconfig("mpdatabase2", "OHMP-Routing");
            _warehouse1 = OtherStuff.LoadXMLconfig("mpwarehouse1", "OHMP-Routing");
            _warehouse2 = OtherStuff.LoadXMLconfig("mpwarehouse2", "OHMP-Routing");
            _idleDelay = (OtherStuff.LoadXMLconfig("idledelay", "OHMP-Routing") == "") ? 5 : Convert.ToDecimal(OtherStuff.LoadXMLconfig("idledelay", "OHMP-Routing"));
            mpversion = OtherStuff.LoadXMLconfig("mpversion", "OHMP-Routing");
        }

    }
}