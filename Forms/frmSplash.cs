using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;

namespace OHMPRoutingService.Forms
{
    public partial class frmSplash : Form
    {
        private bool loaded = false;

        public frmSplash()
        {
            InitializeComponent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOHMPR));
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.Icon = Icon.FromHandle(((Bitmap)imageList1.Images[0]).GetHicon());
            this.Opacity = 0;
            this.TopMost = true;
            lblVersion.Text = String.Concat("V",
                Assembly.GetExecutingAssembly().GetName().Version.Major.ToString(),
                ".",
                Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString(),
                ".",
                Assembly.GetExecutingAssembly().GetName().Version.Build.ToString());
        }

        /// <summary>
        /// Status text to display on the splash screen
        /// </summary>
        public string StatusText
        {
            set
            {
                lblProgress.Text = value;
                lblProgress.Visible = true;
                Application.DoEvents();
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// True when the form is shown and ready to display status messages
        /// </summary>
        public bool Loaded
        {
            get { return loaded; }
        }

        private void frmSplash_Shown(object sender, EventArgs e)
        {
            OtherStuff.FadeForm(this, 100, true);
            loaded = true;
        }

        /// <summary>
        /// fade the splash screen and close
        /// </summary>
        public void FadeOut()
        {
            OtherStuff.FadeForm(this, 100, false);
            this.Close();
        }

    }
}