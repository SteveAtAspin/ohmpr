using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.ServiceProcess;
using OHMPRoutingService;

namespace OHMPRoutingService.Forms
{
    public partial class frmOHMPR : Form
    {
        #region Declarations
        private string server = "";
        private string authentication = "";
        private string username = "";
        private string password = "";
        private string ohdatabase = "";
        private string mpdatabase1 = "";
        private string mpdatabase2 = "";
        private string warehouse1 = "";
        private string warehouse2 = "";
        private string mpversion = "";
        private bool SettingsAmended = false;
        private frmSplash splashScreen = new frmSplash();
        private bool btextchanged = false;

        #endregion

        public frmOHMPR()
        {
            this.Visible = false;
            this.Cursor = Cursors.WaitCursor;
            splashScreen.Show();
            while (!splashScreen.Loaded)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
            }
            System.Threading.Thread.Sleep(500);
            splashScreen.StatusText = "Initializing Components";
            InitializeComponent();
            splashScreen.StatusText = "Checking folders";
            if (Directory.Exists(Constants.appDatapathNoSlash) == false)
                Directory.CreateDirectory(Constants.appDatapathNoSlash);
            if (Directory.Exists(Constants.appDatapathWithSlash + "errfiles") == false)
                Directory.CreateDirectory(Constants.appDatapathWithSlash + "errfiles");
            if (Directory.Exists(Constants.appDatapathWithSlash + "logfiles") == false)
                Directory.CreateDirectory(Constants.appDatapathWithSlash + "logfiles");
            if (Directory.Exists(Constants.appDatapathWithSlash + "temp") == false)
                Directory.CreateDirectory(Constants.appDatapathWithSlash + "temp");
            splashScreen.StatusText = "Initializing Controls";
            if ((OtherStuff.LoadXMLconfig("server", "OHMP-Routing")).Length > 0 &&
                (OtherStuff.LoadXMLconfig("server", "OHMP-Routing")).ToLower() != Constants.defaultServer.ToLower())
            {
                cmbOHServer.Items.Add(OtherStuff.LoadXMLconfig("server", "OHMP-Routing"));
            }
            splashScreen.StatusText = "Populating SQL Server List";
            PopulateServerCombo(cmbOHServer);
            cmbAuthentication.Items.Add("Windows Authentication");
            cmbAuthentication.Items.Add("SQL Server Authentication");

            lblServiceStatus.Text = getCurrentServiceStatus();

            lblVersion.Text = "V" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.ShowInTaskbar = true;
            splashScreen.StatusText = "Loading Configuration";
            LoadXML();
            
            SetEvents(this);
            this.Visible = false;
            splashScreen.StatusText = "Console Ready";
            this.Shown += DisplayMe;
            splashScreen.FadeOut();
            this.Cursor = Cursors.Default;
            Application.DoEvents();
        }

        #region Events

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            OtherStuff.FadeForm(this, 50, false);
            this.Cursor = Cursors.WaitCursor;
            splashScreen = new frmSplash();
            splashScreen.Show();
            splashScreen.StatusText = "Saving Configuration";
            bool bAsk = SettingsAmended;
            SaveXML(); // Just in case

            if (getCurrentServiceStatus() == "Running" && bAsk && MessageBox.Show("Do you want to restart the service", "Settings Amended", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (getCurrentServiceStatus() == "Running")
                {
                    restartService();
                    splashScreen.StatusText = "Restarting Service";
                }
                else
                {   // leave this in case someone stopped it while the messagebox was displayed
                    splashScreen.StatusText = "Starting Service";
                    startService();
                }
            }
            splashScreen.StatusText = "";
            splashScreen.FadeOut();
        }

        private void dtpStartTime_ValueChanged(object sender, EventArgs e)
        {
            if (dtpStartTime.Value > dtpEndTime.Value)
                dtpEndTime.Value = dtpStartTime.Value;
        }

        private void dtpEndTime_ValueChanged(object sender, EventArgs e)
        {
            if (dtpEndTime.Value < dtpStartTime.Value)
                dtpStartTime.Value = dtpEndTime.Value;
        }

        private void frmMain_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == false)
                TickToc.Enabled = true;
        }
                
        private void UserDidSomethingSoSaveXML(object sender, EventArgs e)
        {
            if (btextchanged) // Only save if something actually changed
            {
                this.Cursor = Cursors.WaitCursor;
                SaveXML();
                btextchanged = false;
                this.Cursor = Cursors.Default;
            }
        }

        private void UserAlteredField(object sender, EventArgs e)
        {
            btextchanged = true;
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnServiceStart_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            startService();
            lblServiceStatus.Text = getCurrentServiceStatus();
            setServiceButtonEnablement();
            SettingsAmended = false;
            this.Cursor = Cursors.Default;
        }

        private void btnServiceStop_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            stopService();
            lblServiceStatus.Text = getCurrentServiceStatus();
            setServiceButtonEnablement();
            SettingsAmended = false;
            this.Cursor = Cursors.Default;
        }

        private void btnServiceRestart_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            restartService();
            lblServiceStatus.Text = getCurrentServiceStatus();
            setServiceButtonEnablement();

            SettingsAmended = false;
            this.Cursor = Cursors.Default;
        }

        private void installToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool b = false;
            try
            {
                ServiceHelper sh = new ServiceHelper();

                b = sh.InstallService("\"" + System.Reflection.Assembly.GetExecutingAssembly().CodeBase.Substring(8).Replace(@"/", @"\") + "\""
                , Constants.serviceName
                , Constants.serviceDescription
                , false
                );
                sh = null;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, Constants.serviceDescription, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1); }

            if (b)
            {
                #region Add service description
                // We need to add a description because Microsoft forgot it in the InstallService
                // method of ServiceHelper
                Microsoft.Win32.RegistryKey ServiceDescription = null;

                try
                {
                    ServiceDescription =
                    Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Services\" + Constants.serviceName, true);
                    ServiceDescription.SetValue("Description", "Routes Minipick data from Dimensions to 2 Minipick databases");
                    ServiceDescription.Close();

                }
                catch (Exception)
                {

                }
                #endregion
                this.TopMost = false;
                OtherStuff.TimedUserPrompt("Service '" + Constants.serviceName + "' Service installed", Constants.serviceDescription, 0, 1, 5, 1, "Ok", "", "");
                this.TopMost = true;
            }
            else
                MessageBox.Show("Service '" + Constants.serviceName + "' Service failed to install", Constants.serviceDescription, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            lblServiceStatus.Text = getCurrentServiceStatus();
            setServiceButtonEnablement();
        }

        private void uninstallServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool b = false;
            try
            {
                ServiceHelper sh = new ServiceHelper();
                ServiceController sc = new ServiceController(Constants.serviceName);
                if (sc.CanStop)
                    sc.Stop();
                sc.Close();
                b = sh.UnInstallService(Constants.serviceName);
                sh = null;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message, Constants.serviceDescription, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1); }
            if (b)
            {
                this.TopMost = false;
                OtherStuff.TimedUserPrompt("Service '" + Constants.serviceName +"' uninstalled", Constants.serviceDescription, 0, 1, 5, 1, "Ok", "", "");
                this.Close();   // Must end or service stays as installed but unuseable
            }
            else
                MessageBox.Show("Service '" + Constants.serviceName + "' failed to uninstall", Constants.serviceDescription, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            lblServiceStatus.Text = getCurrentServiceStatus();
            setServiceButtonEnablement();
        }

        private void TickToc_Tick(object sender, EventArgs e)
        {
            lblServiceStatus.Text = getCurrentServiceStatus();
            setServiceButtonEnablement();
        }

        private void checkConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            SaveXML();
            try
            {
                SqlConnection _ohconn = new SqlConnection();
                SqlConnection _mpconn1 = new SqlConnection();
                SqlConnection _mpconn2 = new SqlConnection();
                _ohconn = CreateConnection(server, authentication, ohdatabase, username, password);
                _ohconn.Open();
                _mpconn1 = CreateConnection(server, authentication, mpdatabase1, username, password);
                _mpconn1.Open();
                _mpconn2 = CreateConnection(server, authentication, mpdatabase2, username, password);
                _mpconn2.Open();

                if (OtherStuff.LoadXMLconfig("ohdatabase", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpdatabase1", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpdatabase2", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("server", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("idledelay", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpwarehouse1", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpwarehouse2", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpversion", "OHMP-Routing") == "")
                {
                    throw new Exception( "Missing Parameters");
                }
                

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                OtherStuff.TimedUserPrompt("Invalid config.\r\n\r\n" + ex.Message, "Invalid Config", 1, 1, 10, 1, "OK", "", "");
                return;
            }
            this.Cursor = Cursors.Default;
            OtherStuff.TimedUserPrompt("Databases can be opened and values are in the parameters.", "Config check", 0, 1, 5, 1, "Ok", "", "");
        }

        private void DisplayMe(object sender, EventArgs e)
        {
            this.Opacity = 100;
            this.Visible = true;
            OtherStuff.FadeForm(this, 50, true);
        }

        #endregion

        #region Private Methods
       
        private static SqlConnection CreateConnection(string server, string security, string database, string user, string pass)
        {
            SqlConnection con = new SqlConnection();
            if (security == "Windows Authentication")
            {
                con.ConnectionString = string.Format(
                    @"Server={0}; Integrated security={1}; Initial Catalog={2}; MultipleActiveResultSets=True; Application Name=" + Constants.serviceName,
                    server, "SSPI", database);
            }
            else
            {
                con.ConnectionString = string.Format(
                    @"Server={0}; Initial Catalog={1}; MultipleActiveResultSets=True; User Id={2}; Password={3}; Application Name=" + Constants.serviceName,
                    server, database, user, pass);
            }
            return con;
        }
                
        private void LoadXML()
        {           
            dtpStartTime.Value = (OtherStuff.LoadXMLconfig("StartTime", "OHMP-Routing") == "") ? Convert.ToDateTime("00:00") : Convert.ToDateTime(OtherStuff.LoadXMLconfig("StartTime", "OHMP-Routing"));
            dtpEndTime.Value = (OtherStuff.LoadXMLconfig("EndTime", "OHMP-Routing") == "") ? Convert.ToDateTime("23:59") : Convert.ToDateTime(OtherStuff.LoadXMLconfig("EndTime", "OHMP-Routing"));
            cmbOHServer.Text = OtherStuff.LoadXMLconfig("server", "OHMP-Routing");
            server = cmbOHServer.Text;
            cmbAuthentication.Text = OtherStuff.LoadXMLconfig("authentication", "OHMP-Routing");
            authentication = cmbAuthentication.Text;
            txtUserName.Text = (OtherStuff.LoadXMLconfig("username", "OHMP-Routing") == "") ? "" : Crypto.Decrypt(OtherStuff.LoadXMLconfig("username", "OHMP-Routing"), "OHMP-Routing");
            username = txtUserName.Text;
            txtPassword.Text = (OtherStuff.LoadXMLconfig("password", "OHMP-Routing") == "") ? "" : Crypto.Decrypt(OtherStuff.LoadXMLconfig("password", "OHMP-Routing"), "OHMP-Routing");
            password = txtPassword.Text;
            txtOHDatabaseName.Text = OtherStuff.LoadXMLconfig("ohdatabase", "OHMP-Routing");
            ohdatabase = txtOHDatabaseName.Text;
            txtMPDatabase1.Text = OtherStuff.LoadXMLconfig("mpdatabase1", "OHMP-Routing");
            mpdatabase1 = txtMPDatabase1.Text;
            txtMPDatabase2.Text = OtherStuff.LoadXMLconfig("mpdatabase2", "OHMP-Routing");
            mpdatabase2 = txtMPDatabase2.Text;
            txtWHS1.Text = OtherStuff.LoadXMLconfig("mpwarehouse1", "OHMP-Routing");
            warehouse1 =txtWHS1.Text;
            txtWHS2.Text = OtherStuff.LoadXMLconfig("mpwarehouse2", "OHMP-Routing");
            warehouse2 = txtWHS2.Text;
            txtVersion.Text = OtherStuff.LoadXMLconfig("mpversion", "OHMP-Routing");
            mpversion = txtVersion.Text;
            nudIdleDelay.Value = (OtherStuff.LoadXMLconfig("idledelay", "OHMP-Routing") == "") ? 5 : Convert.ToDecimal(OtherStuff.LoadXMLconfig("idledelay", "OHMP-Routing"));
            
        }

        private void SaveXML()
        {           
            OtherStuff.WriteXMLConfig("StartTime", dtpStartTime.Value.ToShortTimeString(), "OHMP-Routing");
            OtherStuff.WriteXMLConfig("EndTime", dtpEndTime.Value.ToShortTimeString(), "OHMP-Routing");
            OtherStuff.WriteXMLConfig("server", cmbOHServer.Text, "OHMP-Routing");
            server = cmbOHServer.Text;
            OtherStuff.WriteXMLConfig("authentication", cmbAuthentication.Text, "OHMP-Routing");
            authentication = cmbAuthentication.Text;
            OtherStuff.WriteXMLConfig("username", Crypto.Encrypt(txtUserName.Text, "OHMP-Routing"), "OHMP-Routing");
            username = txtUserName.Text;
            OtherStuff.WriteXMLConfig("password", Crypto.Encrypt(txtPassword.Text, "OHMP-Routing"), "OHMP-Routing");
            password = txtPassword.Text;
            OtherStuff.WriteXMLConfig("ohdatabase", txtOHDatabaseName.Text, "OHMP-Routing");
            ohdatabase = txtOHDatabaseName.Text;
            OtherStuff.WriteXMLConfig("mpdatabase1", txtMPDatabase1.Text, "OHMP-Routing");
            mpdatabase1 = txtMPDatabase1.Text;
            OtherStuff.WriteXMLConfig("mpdatabase2", txtMPDatabase2.Text, "OHMP-Routing");
            mpdatabase2 = txtMPDatabase2.Text;
            OtherStuff.WriteXMLConfig("mpwarehouse1", txtWHS1.Text, "OHMP-Routing");
            warehouse1 = txtWHS1.Text;
            OtherStuff.WriteXMLConfig("mpwarehouse2", txtWHS2.Text, "OHMP-Routing");
            warehouse2 = txtWHS2.Text;
            OtherStuff.WriteXMLConfig("idledelay", nudIdleDelay.Value.ToString(), "OHMP-Routing");
            OtherStuff.WriteXMLConfig("mpversion", txtVersion.Text, "OHMP-Routing");
            mpversion = txtVersion.Text;
            SettingsAmended = true;
        }

        private void SetEvents(System.Windows.Forms.Control cIn)
        {
            foreach (System.Windows.Forms.Control c in cIn.Controls)
            {
                if ((c is TextBox) || (c is NumericUpDown) || (c is DateTimePicker) || c is ComboBox)
                {
                    c.TextChanged += UserAlteredField;
                    c.Leave += UserDidSomethingSoSaveXML;
                }

                if (c.Controls.Count > 0)
                    SetEvents(c);
            }
        }
        
        private static string getCurrentServiceStatus()
        {
            string retString = "";

            try
            {
                ServiceController ASService = new ServiceController(Constants.serviceName);
                ASService.Refresh();
                switch (ASService.Status)
                {
                    case ServiceControllerStatus.Running: retString = "Running"; break;
                    case ServiceControllerStatus.Stopped: retString = "Stopped"; break;
                    case ServiceControllerStatus.Paused: retString = "Paused"; break;
                    case ServiceControllerStatus.StartPending: retString = "Starting"; break;
                    case ServiceControllerStatus.StopPending: retString = "Stopping"; break;
                    default: retString = "Uninstalled"; break;
                }
            }
            catch
            {
                retString = "Uninstalled";
            }

            return retString;
        }

        private void setServiceButtonEnablement()
        {
            if (lblServiceStatus.Text == "Running")
            {
                btnServiceStart.Enabled = false;
                btnServiceStop.Enabled = true;
                btnServiceRestart.Enabled = true;
                installToolStripMenuItem.Visible = false;
                uninstallServiceToolStripMenuItem.Visible = false;
            }
            if (lblServiceStatus.Text == "Stopped")
            {
                btnServiceStart.Enabled = true;
                btnServiceStop.Enabled = false;
                btnServiceRestart.Enabled = false;
                installToolStripMenuItem.Visible = false;
                uninstallServiceToolStripMenuItem.Visible = true;
            }
            if (lblServiceStatus.Text == "Uninstalled")
            {
                btnServiceStart.Enabled = false;
                btnServiceStop.Enabled = false;
                btnServiceRestart.Enabled = false;
                installToolStripMenuItem.Visible = true;
                uninstallServiceToolStripMenuItem.Visible = false;
            }
        }
       
        private void startService()
        {
            try
            {
                disableServiceButtons();
                lblServiceStatus.Text = "Starting";
                Application.DoEvents();

                ServiceController ohmprService = new ServiceController(Constants.serviceName);
                ohmprService.Start();

                while (ohmprService.Status == ServiceControllerStatus.StartPending)
                {
                    ohmprService.Refresh();
                }

                ohmprService = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Service failed to start\r\n" + ex.Message + "\r\n\r\nCheck the config and\r\ntry starting the console as administrator", "Start Problem", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                setServiceButtonEnablement();
            }
        }

        private void stopService()
        {
            try
            {
                disableServiceButtons();
                lblServiceStatus.Text = "Stopping";
                Application.DoEvents();

                ServiceController VPService = new ServiceController(Constants.serviceName);
                VPService.Stop();

                while (VPService.Status == ServiceControllerStatus.StopPending)
                {
                    VPService.Refresh();
                }

                VPService = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Service failed to stop\r\n" + ex.Message + "\r\n\r\nCheck the config and\r\ntry starting the console as administrator", "Stop Problem", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                setServiceButtonEnablement();
            }
        }

        private void restartService()
        {
            stopService();
            lblServiceStatus.Text = getCurrentServiceStatus();
            Application.DoEvents();
            System.Threading.Thread.Sleep(500);
            startService();
        }

        private void disableServiceButtons()
        {
            btnServiceStart.Enabled = false;
            btnServiceStop.Enabled = false;
            btnServiceRestart.Enabled = false;
            installToolStripMenuItem.Visible = false;
            uninstallServiceToolStripMenuItem.Visible = false;
        }

        private void PopulateServerCombo(System.Windows.Forms.ComboBox cmbSvr)
        {
            cmbSvr.Items.Add(Constants.defaultServer);
            
            System.Data.Sql.SqlDataSourceEnumerator instance = System.Data.Sql.SqlDataSourceEnumerator.Instance;
            System.Data.DataTable dataTable = instance.GetDataSources();
            foreach (System.Data.DataRow row in dataTable.Rows)
            {
                if ((row.ItemArray[0] + @"\" + row.ItemArray[1]) != Constants.defaultServer)
                {
                    if (row.ItemArray[0] + @"\" + row.ItemArray[1] != Constants.defaultServer)
                    cmbSvr.Items.Add(row.ItemArray[0] + @"\" + row.ItemArray[1]);
                }
            }
        }

        #endregion

        


    }
}