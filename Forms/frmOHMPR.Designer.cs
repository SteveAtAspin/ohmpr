namespace OHMPRoutingService.Forms
{
    partial class frmOHMPR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOHMPR));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnServiceRestart = new System.Windows.Forms.Button();
            this.btnServiceStop = new System.Windows.Forms.Button();
            this.btnServiceStart = new System.Windows.Forms.Button();
            this.lblServiceStatus = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uninstallServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.installToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOHDatabaseName = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.cmbAuthentication = new System.Windows.Forms.ComboBox();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.TickToc = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.cmbOHServer = new System.Windows.Forms.ComboBox();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.nudIdleDelay = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMPDatabase1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMPDatabase2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtWHS1 = new System.Windows.Forms.TextBox();
            this.txtWHS2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIdleDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnServiceRestart);
            this.groupBox4.Controls.Add(this.btnServiceStop);
            this.groupBox4.Controls.Add(this.btnServiceStart);
            this.groupBox4.Controls.Add(this.lblServiceStatus);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(48, 471);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(647, 116);
            this.groupBox4.TabIndex = 200;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Windows Service Control";
            // 
            // btnServiceRestart
            // 
            this.btnServiceRestart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServiceRestart.Location = new System.Drawing.Point(368, 67);
            this.btnServiceRestart.Margin = new System.Windows.Forms.Padding(4);
            this.btnServiceRestart.Name = "btnServiceRestart";
            this.btnServiceRestart.Size = new System.Drawing.Size(100, 28);
            this.btnServiceRestart.TabIndex = 160;
            this.btnServiceRestart.Text = "Restart";
            this.btnServiceRestart.UseVisualStyleBackColor = true;
            this.btnServiceRestart.Click += new System.EventHandler(this.btnServiceRestart_Click);
            // 
            // btnServiceStop
            // 
            this.btnServiceStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServiceStop.Location = new System.Drawing.Point(248, 67);
            this.btnServiceStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnServiceStop.Name = "btnServiceStop";
            this.btnServiceStop.Size = new System.Drawing.Size(100, 28);
            this.btnServiceStop.TabIndex = 150;
            this.btnServiceStop.Text = "Stop";
            this.btnServiceStop.UseVisualStyleBackColor = true;
            this.btnServiceStop.Click += new System.EventHandler(this.btnServiceStop_Click);
            // 
            // btnServiceStart
            // 
            this.btnServiceStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServiceStart.Location = new System.Drawing.Point(127, 67);
            this.btnServiceStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnServiceStart.Name = "btnServiceStart";
            this.btnServiceStart.Size = new System.Drawing.Size(100, 28);
            this.btnServiceStart.TabIndex = 140;
            this.btnServiceStart.Text = "Start";
            this.btnServiceStart.UseVisualStyleBackColor = true;
            this.btnServiceStart.Click += new System.EventHandler(this.btnServiceStart_Click);
            // 
            // lblServiceStatus
            // 
            this.lblServiceStatus.AutoSize = true;
            this.lblServiceStatus.Location = new System.Drawing.Point(402, 31);
            this.lblServiceStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServiceStatus.Name = "lblServiceStatus";
            this.lblServiceStatus.Size = new System.Drawing.Size(62, 17);
            this.lblServiceStatus.TabIndex = 1;
            this.lblServiceStatus.Text = "[status]";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(157, 31);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(237, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "The Aspin OHMP Routing service is:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(734, 28);
            this.menuStrip1.TabIndex = 96;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkConfigToolStripMenuItem,
            this.uninstallServiceToolStripMenuItem,
            this.installToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // checkConfigToolStripMenuItem
            // 
            this.checkConfigToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("checkConfigToolStripMenuItem.Image")));
            this.checkConfigToolStripMenuItem.Name = "checkConfigToolStripMenuItem";
            this.checkConfigToolStripMenuItem.Size = new System.Drawing.Size(251, 24);
            this.checkConfigToolStripMenuItem.Text = "Check Config";
            this.checkConfigToolStripMenuItem.Click += new System.EventHandler(this.checkConfigToolStripMenuItem_Click);
            // 
            // uninstallServiceToolStripMenuItem
            // 
            this.uninstallServiceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("uninstallServiceToolStripMenuItem.Image")));
            this.uninstallServiceToolStripMenuItem.Name = "uninstallServiceToolStripMenuItem";
            this.uninstallServiceToolStripMenuItem.Size = new System.Drawing.Size(251, 24);
            this.uninstallServiceToolStripMenuItem.Text = "&Uninstall Windows Service";
            this.uninstallServiceToolStripMenuItem.Click += new System.EventHandler(this.uninstallServiceToolStripMenuItem_Click);
            // 
            // installToolStripMenuItem
            // 
            this.installToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("installToolStripMenuItem.Image")));
            this.installToolStripMenuItem.Name = "installToolStripMenuItem";
            this.installToolStripMenuItem.Size = new System.Drawing.Size(251, 24);
            this.installToolStripMenuItem.Text = "&Install Windows Service";
            this.installToolStripMenuItem.Click += new System.EventHandler(this.installToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem1.Image")));
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(251, 24);
            this.exitToolStripMenuItem1.Text = "E&xit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 82;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(13, 228);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(284, 17);
            this.label5.TabIndex = 45;
            this.label5.Text = "Dimensions MP Database name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtOHDatabaseName
            // 
            this.txtOHDatabaseName.Location = new System.Drawing.Point(303, 225);
            this.txtOHDatabaseName.Margin = new System.Windows.Forms.Padding(4);
            this.txtOHDatabaseName.Name = "txtOHDatabaseName";
            this.txtOHDatabaseName.Size = new System.Drawing.Size(284, 22);
            this.txtOHDatabaseName.TabIndex = 50;
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(49, 181);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(107, 17);
            this.lblPassword.TabIndex = 35;
            this.lblPassword.Text = "Password";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(13, 90);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "SQL Server name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(45, 118);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Authentication";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUsername
            // 
            this.lblUsername.Location = new System.Drawing.Point(48, 151);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(108, 17);
            this.lblUsername.TabIndex = 25;
            this.lblUsername.Text = "User name";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbAuthentication
            // 
            this.cmbAuthentication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAuthentication.FormattingEnabled = true;
            this.cmbAuthentication.Location = new System.Drawing.Point(302, 118);
            this.cmbAuthentication.Margin = new System.Windows.Forms.Padding(4);
            this.cmbAuthentication.Name = "cmbAuthentication";
            this.cmbAuthentication.Size = new System.Drawing.Size(398, 24);
            this.cmbAuthentication.TabIndex = 20;
            // 
            // lblStartTime
            // 
            this.lblStartTime.Location = new System.Drawing.Point(13, 433);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(158, 24);
            this.lblStartTime.TabIndex = 105;
            this.lblStartTime.Text = "Data Scan Start Time";
            this.lblStartTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TickToc
            // 
            this.TickToc.Enabled = true;
            this.TickToc.Interval = 10000;
            this.TickToc.Tick += new System.EventHandler(this.TickToc_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Asset-1.ico");
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(303, 181);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(272, 22);
            this.txtPassword.TabIndex = 40;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(302, 151);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(272, 22);
            this.txtUserName.TabIndex = 30;
            // 
            // cmbOHServer
            // 
            this.cmbOHServer.FormattingEnabled = true;
            this.cmbOHServer.Location = new System.Drawing.Point(303, 87);
            this.cmbOHServer.Margin = new System.Windows.Forms.Padding(4);
            this.cmbOHServer.Name = "cmbOHServer";
            this.cmbOHServer.Size = new System.Drawing.Size(398, 24);
            this.cmbOHServer.TabIndex = 10;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEndTime.Location = new System.Drawing.Point(381, 435);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            this.dtpEndTime.Size = new System.Drawing.Size(100, 22);
            this.dtpEndTime.TabIndex = 120;
            this.dtpEndTime.ValueChanged += new System.EventHandler(this.dtpEndTime_ValueChanged);
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(54, 45);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(165, 22);
            this.lblVersion.TabIndex = 83;
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEndTime
            // 
            this.lblEndTime.Location = new System.Drawing.Point(286, 433);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(85, 24);
            this.lblEndTime.TabIndex = 115;
            this.lblEndTime.Text = "End Time";
            this.lblEndTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpStartTime.Location = new System.Drawing.Point(180, 436);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowUpDown = true;
            this.dtpStartTime.Size = new System.Drawing.Size(100, 22);
            this.dtpStartTime.TabIndex = 110;
            this.dtpStartTime.ValueChanged += new System.EventHandler(this.dtpStartTime_ValueChanged);
            // 
            // nudIdleDelay
            // 
            this.nudIdleDelay.Location = new System.Drawing.Point(597, 435);
            this.nudIdleDelay.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudIdleDelay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudIdleDelay.Name = "nudIdleDelay";
            this.nudIdleDelay.Size = new System.Drawing.Size(52, 22);
            this.nudIdleDelay.TabIndex = 130;
            this.nudIdleDelay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(521, 437);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 125;
            this.label3.Text = "Idle Delay";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(13, 261);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(284, 17);
            this.label4.TabIndex = 55;
            this.label4.Text = "MP one Database name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMPDatabase1
            // 
            this.txtMPDatabase1.Location = new System.Drawing.Point(303, 258);
            this.txtMPDatabase1.Margin = new System.Windows.Forms.Padding(4);
            this.txtMPDatabase1.Name = "txtMPDatabase1";
            this.txtMPDatabase1.Size = new System.Drawing.Size(284, 22);
            this.txtMPDatabase1.TabIndex = 60;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(13, 334);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(284, 17);
            this.label6.TabIndex = 75;
            this.label6.Text = "MP two Database name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMPDatabase2
            // 
            this.txtMPDatabase2.Location = new System.Drawing.Point(303, 331);
            this.txtMPDatabase2.Margin = new System.Windows.Forms.Padding(4);
            this.txtMPDatabase2.Name = "txtMPDatabase2";
            this.txtMPDatabase2.Size = new System.Drawing.Size(284, 22);
            this.txtMPDatabase2.TabIndex = 80;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(657, 437);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 17);
            this.label7.TabIndex = 132;
            this.label7.Text = "seconds";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(70, 298);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(227, 18);
            this.label8.TabIndex = 65;
            this.label8.Text = "Minipick one freetype30 value";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWHS1
            // 
            this.txtWHS1.Location = new System.Drawing.Point(303, 296);
            this.txtWHS1.Name = "txtWHS1";
            this.txtWHS1.Size = new System.Drawing.Size(143, 22);
            this.txtWHS1.TabIndex = 70;
            // 
            // txtWHS2
            // 
            this.txtWHS2.Location = new System.Drawing.Point(303, 368);
            this.txtWHS2.Name = "txtWHS2";
            this.txtWHS2.Size = new System.Drawing.Size(143, 22);
            this.txtWHS2.TabIndex = 90;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(70, 370);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(227, 18);
            this.label9.TabIndex = 85;
            this.label9.Text = "Minipick two freetype30 value";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(303, 397);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(143, 22);
            this.txtVersion.TabIndex = 100;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(70, 399);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(227, 18);
            this.label10.TabIndex = 95;
            this.label10.Text = "Minipick version number";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmOHMPR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 600);
            this.Controls.Add(this.txtVersion);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtWHS2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtWHS1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMPDatabase2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMPDatabase1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudIdleDelay);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtOHDatabaseName);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.cmbAuthentication);
            this.Controls.Add(this.lblStartTime);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.cmbOHServer);
            this.Controls.Add(this.dtpEndTime);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblEndTime);
            this.Controls.Add(this.dtpStartTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmOHMPR";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aspin OHMPR Service Control";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIdleDelay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnServiceRestart;
        private System.Windows.Forms.Button btnServiceStop;
        private System.Windows.Forms.Button btnServiceStart;
        private System.Windows.Forms.Label lblServiceStatus;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOHDatabaseName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.ComboBox cmbAuthentication;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Timer TickToc;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.ComboBox cmbOHServer;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblEndTime;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.ToolStripMenuItem installToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uninstallServiceToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown nudIdleDelay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMPDatabase1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMPDatabase2;
        private System.Windows.Forms.ToolStripMenuItem checkConfigToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtWHS1;
        private System.Windows.Forms.TextBox txtWHS2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label10;
    }
}