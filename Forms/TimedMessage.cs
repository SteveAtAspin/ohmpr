using System;
using System.Drawing;
using System.Windows.Forms;

namespace OHMPRoutingService
{
    public partial class TimedMessage : Form
    {
        #region Declarations
        ImageList imageList1 = new ImageList();
        private int idelay = 5;
        private int icountdown = 0;
        private int defaultButton = 3;
        private int userResponse = -1;
        private int iautoclose = 0;
        private string yesbuttontext = "&Yes";
        private string nobuttontext = "&No";
        private string cancelbuttontext = "&Cancel";
        private int displaybuttons = 5;
        #endregion

        public TimedMessage()
        {
            InitializeComponent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Forms.frmOHMPR));
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.Icon = Icon.FromHandle(((Bitmap)imageList1.Images[0]).GetHicon());
        }

        #region Public Properties
        /// <summary>
        /// The message to display to the user
        /// </summary>
        public string message
        {
            set
            {
                this.lblMessage.Text = value;
                resize(); 
            }
        }

        /// <summary>
        /// The message window caption
        /// </summary>
        public string caption
        {
            set { this.Text = value; }
        }

        /// <summary>
        /// The delay in seconds before the buttons are activated
        /// </summary>
        public int delay
        {
            set { idelay = value;
            if (idelay > 0)
            {
                btnYes.Text = "( " + idelay.ToString() + " )";
                btnNo.Text = "( " + idelay.ToString() + " )";
                btnCancel.Text = "( " + idelay.ToString() + " )";
            }
        }
        }

        /// <summary>
        /// Gets the button clicked by the user. Sets the default button. 
        /// 1=Yes, 2=No, 3=Cancel
        /// </summary>
        public int button
        {
            set
            {
                userResponse = value;
                this.AcceptButton = null;
                defaultButton = userResponse;
                switch (userResponse)
                {
                    case 1: this.AcceptButton = btnYes;
                        break;
                    case 2: this.AcceptButton = btnNo;
                        break;
                    case 3: this.AcceptButton = btnCancel;
                        break;
                }
            }
            get { return userResponse; }
        }

        /// <summary>
        /// Sets the time in seconds to autoclose the prompt form. 
        /// Defaults to zero (never close)
        /// </summary>
        public int autoclose
        {
            set { iautoclose = value; }
        }

        /// <summary>
        /// Set the text for button 1
        /// </summary>
        public string yesButtonText
        {
            set { yesbuttontext = value; }
        }

        /// <summary>
        /// Set the text for button 2
        /// </summary>
        /// 
        public string noButtonText
        {
            set { nobuttontext = value; }
        }

        /// <summary>
        /// Set the text for button 3
        /// </summary>
        public string cancelButtonText
        {
            set { cancelbuttontext = value; }
        }

        /// <summary>
        /// Bitflag integer for which buttons to display
        /// bit 1 = Yes button
        /// bit 2 = No button
        /// bit 3 = Cancel button
        /// </summary>
        public int displayButtons
        {
            set
            {
                displaybuttons = value;
                btnYes.Visible = (displaybuttons & 1) != 0;
                btnNo.Visible = (displaybuttons & 2) != 0;
                btnCancel.Visible = (displaybuttons & 4) != 0;

            }
        }
        #endregion

        #region Events
        private void TimedMessage_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            jiggleButtons();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            icountdown++;
            if (icountdown == idelay || (idelay == 0 && icountdown == 1))
            {
                btnYes.Text = yesbuttontext;
                btnNo.Text = nobuttontext;
                btnCancel.Text = cancelbuttontext;
                if (iautoclose == 0)
                    timer1.Enabled = false;
                btnYes.Enabled = true;
                btnNo.Enabled = true;
                btnCancel.Enabled = true;
                System.Drawing.FontStyle newFontStyle = this.btnYes.Font.Style ^ FontStyle.Bold; // ^ FontStyle.Underline;
                Font Newfont = new Font(btnYes.Font.FontFamily, btnYes.Font.Size, newFontStyle);
                switch (defaultButton)
                {
                    case 1: this.AcceptButton = btnYes;
                        this.btnYes.Focus();
                        this.btnYes.Font = Newfont;
                        break;
                    case 2: this.AcceptButton = btnNo;
                        this.btnNo.Focus();
                        this.btnNo.Font = Newfont;
                        break;
                    case 3: this.AcceptButton = btnCancel;
                        this.btnCancel.Focus();
                        this.btnCancel.Font = Newfont;
                        break;

                }
            }
            else
            {
                if (icountdown < idelay)
                {
                    btnYes.Text = "( " + Convert.ToString(idelay - icountdown) + " )";
                    btnNo.Text = "( " + Convert.ToString(idelay - icountdown) + " )";
                    btnCancel.Text = "( " + Convert.ToString(idelay - icountdown) + " )";
                }
                else
                {
                    lblCountdown.Text = Convert.ToString(Math.Abs(iautoclose - (icountdown - idelay)));
                }
            }
            if ((icountdown - idelay) == iautoclose && iautoclose != 0)
            {
                this.Close();
            }            
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            userResponse = 1;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            userResponse = 2;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            userResponse = 3;
            this.Close();
        }

        private void resize()
        {
            this.Height = this.lblMessage.Height + this.lblMessage.Top + this.btnCancel.Height + 50;
            this.Width = Math.Max(Math.Max(this.Width, lblMessage.Width + 50), btnYes.Width * 4);
            this.lblMessage.Left = (this.Width / 2) - (lblMessage.Width / 2);
            this.btnCancel.Top = this.lblMessage.Height + this.lblMessage.Top+10;
            this.btnNo.Top = this.btnCancel.Top;
            this.btnYes.Top = this.btnCancel.Top;
            this.Height = btnYes.Height + btnYes.Top + 50;
            btnNo.Left = (this.Width / 2) - (btnNo.Width / 2);
            btnYes.Left = btnNo.Left - btnYes.Width - 10;
            btnCancel.Left = btnNo.Left + btnNo.Width + 10;
            lblCountdown.Left = this.Width - 50;
            lblCountdown.Top = btnCancel.Top + btnCancel.Height - lblCountdown.Height;
        }
        #endregion

        #region Private Methods
        private void jiggleButtons()
        {
            int c = 0;
            foreach (System.Windows.Forms.Control b in this.Controls)
            {
                if (b is System.Windows.Forms.Button)
                {
                    if (b.Visible)
                        c++;
                }
            }
            int i = 1;
            foreach (System.Windows.Forms.Control b in this.Controls)
            {
                if (b is System.Windows.Forms.Button)
                {
                    if (b.Visible)
                    {
                        b.Left = this.Width - (i * ((this.Width / (c + 1))) + (b.Width / 2));
                        i++;
                    }
                }
            }
        }
        #endregion
    }
}