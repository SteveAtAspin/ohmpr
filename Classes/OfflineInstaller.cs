using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using System.IO;

namespace OHMPRoutingService
{
    // http://www.codeproject.com/KB/install/Installation.aspx
    // Taken from:http://msdn2.microsoft.com/en-us/library/system.configuration.configurationmanager.aspx
    // Set 'RunInstaller' attribute to true.
    [RunInstaller(true)]
    public class InstallerClass : System.Configuration.Install.Installer
    {
        public InstallerClass()
            : base()
        {
            // Attach the 'BeforeUnistall' event.
            this.BeforeUninstall += new InstallEventHandler(MyInstaller_BeforeUninstall);
            // Attach the 'Committed' event.
            this.Committed += new InstallEventHandler(MyInstaller_Committed);
            // Attach the 'Committing' event.
            this.Committing += new InstallEventHandler(MyInstaller_Committing);

        }
        // Event handler for 'Committing' event.
        private static void MyInstaller_Committing(object sender, InstallEventArgs e)
        {
            //Console.WriteLine("");
            //Console.WriteLine("Committing Event occured.");
            //Console.WriteLine("");
        }
        // Event handler for 'Committed' event.
        private static void MyInstaller_Committed(object sender, InstallEventArgs e)
        {
            try
            {
                ServiceHelper sh = new ServiceHelper();
                sh.InstallService("\"" + System.Reflection.Assembly.GetExecutingAssembly().CodeBase.Substring(8).Replace(@"/", @"\") + "\""
                , Constants.serviceName
                , Constants.serviceDescription
                , false
                );
                sh = null;
            }
            catch
            {}  // May be already installed so ignore errors
            try
            {
                Process proc = new Process();
                proc.StartInfo.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\OHMPRoutingService.exe";
                proc.StartInfo.Arguments = "/config";
                proc.StartInfo.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                proc.Start();
            }
            catch
            {
                // Do nothing... 
            }
        }
        // Override the 'Install' method.
        public override void Install(IDictionary savedState)
        {
            base.Install(savedState);
        }
        // Override the 'Commit' method.
        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }
        // Override the 'Rollback' method.
        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }
        // Override the 'BeforeUnistall' method
        protected void MyInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            ServiceHelper sh = new ServiceHelper();
            try
            {
                System.ServiceProcess.ServiceController VPService = new System.ServiceProcess.ServiceController(Constants.serviceName);
                VPService.Stop();

                while (VPService.Status == System.ServiceProcess.ServiceControllerStatus.StopPending)
                {
                    VPService.Refresh();
                }

                VPService = null;
            }
            catch
            {

            }
            try
            {
                sh.UnInstallService(Constants.serviceName);
            }
            catch { }
            sh = null;
            
        }
    }

}