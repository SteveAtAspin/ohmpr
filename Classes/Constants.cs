using System;

namespace OHMPRoutingService
{
    class Constants
    {
        #region Path Constants
        public static string appPathWithSlash = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6)
            + @"\";

        public static string appPathNoSlash = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);

        public static string appDatapathWithSlash = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\aspin\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
            + @"\";

        public static string appDatapathNoSlash = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\aspin\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        #endregion

        #region PC Constants
        public static string pcName = System.Environment.MachineName.Trim().Replace(".", "").Replace("/", "").Replace(@"\", "");
        #endregion

        #region SQL Constants

        public static string defaultServer = "localhost\\SQLEXPRESS";
        #endregion

        #region Defaults
        public static string defaultEventSource = "OHMPRoutingService";
        public static string defaultEventLog = "AspinOHMPRouting";
        public static string serviceName = "Aspin_OHMPR";
        public static string serviceDescription = "Aspin OHMPR Service";
        #endregion
    }
}
