using System;
using OHMPRoutingService.xml;

namespace OHMPRoutingService
{
    class OtherStuff
    {/// <summary>
        /// Display a prompt window with a timed delay before accepting input. 
        /// Returns the button selected (1=Yes, 2=No, 3=Cancel)
        /// </summary>
        /// <param name="message">The message text to display</param>
        /// <param name="caption">The window caption</param>
        /// <param name="delay">Delay in seconds before input is permitted</param>
        /// <param name="button">The default button (1=Yes, 2=No, 3=Cancel)</param>
        /// <param name="autoclose">Time in seconds to autoclose message. Zero = no autoclose</param>
        public static int TimedUserPrompt(string message, string caption, int delay, int button, int autoclose)
        {
            return TimedUserPrompt(message, caption, delay, button, autoclose, 7, "&Yes", "&No", "&Cancel");
        }
        /// <summary>
        /// Display a prompt window with a timed delay before accepting input. 
        /// Returns the button selected (1=Yes, 2=No, 3=Cancel)
        /// </summary>
        /// <param name="message">The message text to display</param>
        /// <param name="caption">The window caption</param>
        /// <param name="delay">Delay in seconds before input is permitted</param>
        /// <param name="button">The default button (1=Yes, 2=No, 3=Cancel)</param>
        public static int TimedUserPrompt(string message, string caption, int delay, int button)
        {
            return TimedUserPrompt(message, caption, delay, button, 0, 7, "&Yes", "&No", "&Cancel");
        }
        /// <summary>
        /// Display a prompt window with a timed delay before accepting input. 
        /// Returns the button selected (1=Yes, 2=No, 3=Cancel)
        /// </summary>
        /// <param name="message">The message text to display</param>
        /// <param name="caption">The window caption</param>
        /// <param name="delay">Delay in seconds before input is permitted</param>
        /// <param name="button">The default button (1=Yes, 2=No, 3=Cancel)</param>
        /// <param name="autoclose">Time in seconds to autoclose message. Zero = no autoclose</param>
        /// <param name="buttons">Bitflags for button display</param>
        /// <param name="sCancel">Cancel button text</param>
        /// <param name="sNo">No button text</param>
        /// <param name="sYes">Yes button text</param>
        public static int TimedUserPrompt(string message, string caption, int delay, int button, int autoclose, int buttons, string sYes, string sNo, string sCancel)
        {
            TimedMessage userPrompt = new TimedMessage();
            userPrompt.delay = delay;
            userPrompt.message = message;
            userPrompt.caption = caption;
            userPrompt.button = button;
            userPrompt.autoclose = autoclose;
            userPrompt.displayButtons = buttons;
            userPrompt.yesButtonText = sYes;
            userPrompt.noButtonText = sNo;
            userPrompt.cancelButtonText = sCancel;
            userPrompt.ShowDialog();
            int response = userPrompt.button;
            userPrompt.Close();
            return response;
        }

        /// <summary>
        /// Write parameter to config.xml
        /// </summary>
        /// <param name="sKey">Parameter name</param>
        /// <param name="sVal">Parameter value</param>
        public static void WriteXMLConfig(string sKey, string sVal)
        {
            WriteXMLConfig(sKey, sVal, "OHMP-Routing");
        }
        /// <summary>
        /// Write parameter to config.xml
        /// </summary>
        /// <param name="sKey">Parameter name</param>
        /// <param name="sVal">Parameter value</param>
        /// <param name="sSection">Section (default 'OHMPRoutingService')</param>
        public static void WriteXMLConfig(string sKey, string sVal, string sSection)
        {
            Xml pf = new Xml("OHMPRoutingService.xml");
            try
            {
                pf.Test(true);
            }
            catch (Exception ex)
            {
                TimedUserPrompt("There is a problem saving the configuration\r\n" + ex.Message, "XML Error", 0, 1, 0, 1, "Ok", "", "");
            }
            pf.SetValue(sSection, sKey, sVal);
        }
    
        /// <summary>
        /// Write parameter to config.xml
        /// </summary>
        /// <param name="sKey">Parameter name</param>
        /// <param name="sSection">Section (default 'OHMPRoutingService')</param>
        public static void DeleteXMLConfig(string sKey,  string sSection)
        {
            Xml pf = new Xml("OHMPRoutingService.xml");
            try
            {
                pf.Test(true);
            }
            catch (Exception ex)
            {
                TimedUserPrompt("There is a problem saving the configuration\r\n" + ex.Message, "XML Error", 0, 1, 0, 1, "Ok", "", "");
            }
            pf.RemoveEntry(sSection, sKey);
        }

        public static string LoadXMLconfig(string sKey)
        {
            return LoadXMLconfig(sKey, "OHMP-Routing");
        }
        /// <summary>
        /// Read parameter from XML config file
        /// </summary>
        /// <param name="sKey">Parameter name</param>
        /// <param name="sSection">Section (default 'OHMPRoutingService')</param>
        /// <returns>Parameter value</returns>
        public static string LoadXMLconfig(string sKey, string sSection)
        {
            Xml pf = new Xml("OHMPRoutingService.xml");
            try
            {
                pf.Test(true);
            }
            catch (Exception ex)
            {
                TimedUserPrompt("There is a problem loading the configuration\r\n" + ex.Message, "XML Error", 0, 1, 0, 1, "Ok", "", "");
            }
            string[] entries = pf.GetEntryNames(sSection);

            string s = (pf.HasEntry(sSection, sKey))
                ? pf.GetValue(sSection, sKey).ToString()
                : "";

            return s;
        }

        /// <summary>
        /// Used to fade in/out a form using a user defined number of steps
        /// </summary>
        /// <param name="f">The Windows form to fade out</param>
        /// <param name="NumberOfSteps">The number of steps used to fade the form</param>
        /// <param name="Up">Fade Up (true) or Down (false)</param>
        public static void FadeForm(System.Windows.Forms.Form f, byte NumberOfSteps, bool Up)
        {
            float StepVal = (float)(100f / NumberOfSteps);
            float fOpacity = (Up) ? 0f : 100f;

            for (byte b = 0; b < NumberOfSteps; b++)
            {
                f.Opacity = fOpacity / 100;
                System.Windows.Forms.Application.DoEvents();
                fOpacity += (Up) ? StepVal : -StepVal;
                System.Threading.Thread.Sleep(10);
            }
        }
    }
}
