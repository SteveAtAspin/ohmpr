using System.ServiceProcess;

namespace OHMPRoutingService
{
    static class Program
    {
        [System.STAThread]

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            try
            {
                System.Diagnostics.EventLog ASEventLog = new System.Diagnostics.EventLog();
                if (!System.Diagnostics.EventLog.SourceExists(Constants.defaultEventSource))
                {
                    System.Diagnostics.EventLog.CreateEventSource(Constants.defaultEventSource, Constants.defaultEventLog);
                }
                ASEventLog.Source = Constants.defaultEventSource;
                ASEventLog.Log = Constants.defaultEventLog;
                ASEventLog.MaximumKilobytes = 1024;
                ASEventLog.ModifyOverflowPolicy(System.Diagnostics.OverflowAction.OverwriteAsNeeded, 7);

                System.IO.Directory.CreateDirectory(Constants.appDatapathNoSlash);
                if (args.Length == 0 && System.Environment.UserInteractive == false)
                {
                    if (OtherStuff.LoadXMLconfig("ohdatabase", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpdatabase1", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpdatabase2", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("server", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("idledelay", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpwarehouse1", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpwarehouse2", "OHMP-Routing") == ""
                        || OtherStuff.LoadXMLconfig("mpversion", "OHMP-Routing") == "")
                    {
                        //System.Diagnostics.EventLog ASEventLog = new System.Diagnostics.EventLog(); 
                        if (!System.Diagnostics.EventLog.SourceExists(Constants.defaultEventSource))
                        {
                            System.Diagnostics.EventLog.CreateEventSource(Constants.defaultEventSource, Constants.defaultEventLog);
                        }
                        ASEventLog.Source = Constants.defaultEventSource;
                        ASEventLog.Log = Constants.defaultEventLog;
                        ASEventLog.MaximumKilobytes = 1024;
                        ASEventLog.ModifyOverflowPolicy(System.Diagnostics.OverflowAction.OverwriteAsNeeded, 7);

                        ASEventLog.WriteEntry(Constants.serviceDescription + " Aborted\r\n\r\nInvalid Configuration");
                        return;
                    }

                    // More than one user Service may run within the same process. To add
                    // another service to this process, change the following line to
                    // create a second service object. For example,
                    //
                    //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
                    //
                    ServiceBase[] ServicesToRun = new ServiceBase[] { new OHMPRoutingController() };

                    ServiceBase.Run(ServicesToRun);
                }
                else if (args.Length > 1)
                {
                    System.Windows.Forms.MessageBox.Show("Too many parameters.  Please try again.", Constants.serviceDescription, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                }
                else if (System.Environment.UserInteractive == true)
                {
                    Forms.frmOHMPR config = new OHMPRoutingService.Forms.frmOHMPR();
                    config.ShowDialog();
                    config = null;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Sorry, \"" + args[0] + "\" is not a valid command line parameter.  Please try again.", "OHMP Routing Service", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                }

            }
            catch (System.Exception ex)
            {
                System.Diagnostics.EventLog ASEventLog = new System.Diagnostics.EventLog();
                if (!System.Diagnostics.EventLog.SourceExists(Constants.defaultEventSource))
                {
                    System.Diagnostics.EventLog.CreateEventSource(Constants.defaultEventSource, Constants.defaultEventLog);
                }
                ASEventLog.Source = Constants.defaultEventSource;
                ASEventLog.Log = Constants.defaultEventLog;
                ASEventLog.MaximumKilobytes = 1024;
                ASEventLog.ModifyOverflowPolicy(System.Diagnostics.OverflowAction.OverwriteAsNeeded, 7);

                ASEventLog.WriteEntry(Constants.serviceDescription + " Aborted\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace);
            }
        }
    }
}