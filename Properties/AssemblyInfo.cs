﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OHMPRoutingService")]
[assembly: AssemblyDescription("Aspin Job Number 15329")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Aspin Management Systems")]
[assembly: AssemblyProduct("OHMPRoutingService")]
[assembly: AssemblyCopyright("Copyright © Aspin Management Systems 2020")]
[assembly: AssemblyTrademark("Aspin Management Systems Ltd.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4a90d40b-9176-47d2-b170-9b11640068b2")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.1.2.*")]
[assembly: AssemblyFileVersion("1.1.2")]
